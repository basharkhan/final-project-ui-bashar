import React, { Component } from 'react';

import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Navbar from './Components/Navbar';
import Directors from './Components/Directors';
import './App.css'
import Movies from './Components/Movies';
import Movie from './Components/Movie';
import Update from './Components/Update';

class App extends React.Component {
  render() { 
    return (
      <BrowserRouter>
      <Switch>
        <div className="app" >
          <Navbar />
          <Route exact path="/:id" component={Movies} />
          <Route exact path="/Update/:id" component={Update} />
          <Route exact path="/" component={Directors} />
        </div>
        </Switch>
      </BrowserRouter>
    )
  }
}
 
export default App;