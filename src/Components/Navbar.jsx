import React, { Component } from 'react';

import { Link } from 'react-router-dom';

class Navbar extends React.Component {
    render() { 
        return (
            <nav className="navbar navbar-dark bg-dark" style={{ height: 45 }}>
        <Link to="/" style={{ color: 'inherit', textDecoration: 'inherit'}}>
          <h4 style={{ color: "white", marginLeft: 50 }}>ENTERTAINMENT</h4>
        </Link>
      </nav>
        )
    }
}
 
export default Navbar;