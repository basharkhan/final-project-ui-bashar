import React, { Component } from 'react';

import * as myApi from "./api.jsx";

import Movie from './Movie.jsx';

import "./Movies.css"

class Movies extends React.Component {

    constructor(props) {
        super(props);
    
        this.state = {
          movies: [],
          Title: "",
          Desc: "",
          Genre: "",
          Ratings: null
        };
      }

      // Section for creating movie
      handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
      };


      handleSubmit = async (event) => {
        event.preventDefault();
    
        const resp = await myApi.createMovie(this.state, this.props.match.params.id);
        this.setState({
          movies: [...this.state.movies, resp], Title: "",
          Desc: "",
          Genre: "",
          Ratings: ""
        });
      };
    
      
      
      
    
      // section for fetching Movies

  async fetchMovie(id) {
    const movies = await myApi.getMoviesOfDirector(id);


    this.setState({
      movies,
    });
  }

  componentDidMount() {
    let {id} = this.props.match.params;
    this.fetchMovie(id);
  }

      

     // section for deleting movie

     handleDeleteMovie = async (movieId) => {
      console.log(movieId);
     const deleted = await myApi.deleteMovie(movieId);
     
     this.setState({
       movies: this.state.movies.filter((p) => p.id !== movieId),
     });
   };
      
    
    render() { 
        const { Title, Desc, id, Genre, Ratings, movies } = this.state;
        return (
          <div className="forMovie" >

          
            <div className="d-flex flex-row flex-wrap justify-content-around">
        {movies.map(({ Title, Desc, id, Genre, Ratings }) => (
          <Movie
            Title={Title}
            id={id}
            key={id}
            Desc={Desc}
            Genre={Genre}
            Ratings={Ratings}
            onDeleteMovie={this.handleDeleteMovie}
            // onUpdateDirector={this.handleUpdateDirector}
          />
        ))}

        <form onSubmit={this.handleSubmit}>
          <div className="m-4">
            <label htmlFor="Movie-name">Add Movie attributes</label>
            <input
              type="text"
              value={Title}
              name="Title"
              className="form-control"
              id="board-name"
              onChange={this.handleChange}
              placeholder="Movie Name here"
            />
          </div>
          <div className="mb-3 m-4">
            <input
              type="text"
              value={Desc}
              name="Desc"
              className="form-control"
              id="board-name"
              onChange={this.handleChange}
              placeholder="Description here"
            />
          </div>

          <div className="mb-3 m-4">
            <input
              type="text"
              value={Genre}
              name="Genre"
              className="form-control"
              id="board-name"
              onChange={this.handleChange}
              placeholder="Genre here"
            />
          </div>

          <div className="mb-3 m-4">
            <input
              type="text"
              value={Ratings}
              name="Ratings"
              className="form-control"
              id="board-name"
              onChange={this.handleChange}
              placeholder="Ratings here"
            />
          </div>

          

          <button type="submit" className="btn btn-primary m-4">
            Submit
          </button>
        </form>
        
        </div>
        </div>
        )
    }
}
 
export default Movies;