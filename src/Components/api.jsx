import axios from "axios";

export function getDirectors() {

    return axios
    .get(
        `https://finalprojectapiexpress.herokuapp.com/api/getDirectors`
    )
    .then((res) => res.data)
    
}


export function createDirector(state) {
    return axios
      .post(
        `https://finalprojectapiexpress.herokuapp.com/api/createDirector`, state 
      )
      .then((res) => res.data);
  }


export function deleteDirector(id) {
    return axios
      .delete(
        `https://finalprojectapiexpress.herokuapp.com/api/deleteDirector/${id}`
      )
      .then((res) => res.data);
  }


export function updateDirector(id) {
    return axios
      .put(
        `https://finalprojectapiexpress.herokuapp.com/api/updateDirector/${id}`
      )
      .then((res) => res.data);
  }


//   export function getMovies(id) {

//     return axios
//     .get(
//         `https://finalprojectapiexpress.herokuapp.com/api/getDirectors`
//     )
//     .then((res) => res.data)
    
// }


// Fetching movie of a particular director with director id

export function getMoviesOfDirector(directorId) {

  return axios
  .get(
      `https://finalprojectapiexpress.herokuapp.com/api/getAllMovieWithdirectorId/${directorId}`
  )
  .then((res) => res.data)
  
}

export function createMovie(state, directorId) {
  return axios
    .post(
      `https://finalprojectapiexpress.herokuapp.com/api/createMovie/${directorId}`, state 
    )
    .then((res) => res.data);
}


// Delete a  movie

export function deleteMovie(id) {
  return axios
    .delete(
      `https://finalprojectapiexpress.herokuapp.com/api/deleteMovie/${id}`
    )
    .then((res) => res.data);
}



// /getOneDirector/:id


export function getOneDirector(id) {

  return axios
  .get(
      `https://finalprojectapiexpress.herokuapp.com/api/getOneDirector/${id}`
  )
  .then((res) => res.data)
  
}
