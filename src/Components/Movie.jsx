import React, { Component } from "react";

import { Card, ListGroup, Button } from "react-bootstrap";
// class Movie extends React.Component {
//   render() {
    const Movie = ({ Title, Desc, id, Genre, Ratings, onDeleteMovie }) => {
    return (



<div className="card border-success mb-3 d-flex" style={{maxWidth: "18rem"}}>
        <div className="card-header bg-transparent border-success">Genre: {Genre}</div>
        <div className="card-body text-success">
          <h5 className="card-title">{Title}</h5>
          <p style={{color:"black"}} className="card-text">{Desc}
          </p>
        </div>
        <div className="card-footer bg-transparent border-success">imdb: {Ratings}</div>
         <div className="">

 <Button onClick={() => onDeleteMovie(id)} className="m-4" variant="danger">Delete</Button>

 </div>

      </div>

      
);
  }


export default Movie;

/* <Button onClick={() => onUpdateDirector(id)} className="m-4" variant="info">Edit</Button> */
//       <div className="d-flex flex-column">

//       <div className="card border-success mb-3" style={{maxWidth: "18rem"}}>
//         <div className="card-header bg-transparent border-success">{Genre}</div>
//         <div className="card-body text-success">
//           <h5 className="card-title">{Title}</h5>
//           <p className="card-text">{Desc}
//           </p>
//         </div>
//         <div className="card-footer bg-transparent border-success">imdb: {Ratings}</div>
//       </div>
//       <div className="d-flex">

// <Button onClick={() => onDeleteMovie(id)} className="m-4" variant="danger">Delete</Button>

//       </div>
//       </div>