import React, { Component } from 'react';

class Update extends React.Component {

    constructor(props) {
        super(props);
    
        console.log({props})
        this.state = {
          directors: [],
          name: "",
          oscar: null,
          total_movies: null,
          country: ""
        };
      }
    
    render() { 
        return (<div>
             <form onSubmit={this.handleSubmit}>
          <div className="mb-3" >
            <label style={{color:"white" }} htmlFor="Director-name">zedit Director attributes</label>
            <input
              type="text"
              //value={name}
              name="name"
              className="form-control"
              id="board-name"
              onChange={this.handleChange}
              placeholder="Director Name here"
            />
          </div>
          <div className="mb-3">
            <input
              type="text"
            //   value={oscar}
              name="oscar"
              className="form-control"
              id="board-name"
              onChange={this.handleChange}
              placeholder="Oscar wins here"
            />
          </div>

          <div className="mb-3">
            <input
              type="text"
            //   value={total_movies}
              name="total_movies"
              className="form-control"
              id="board-name"
              onChange={this.handleChange}
              placeholder="Total Movies here"
            />
          </div>
          
          <div className="mb-3">
            <input
              type="text"
            //   value={country}
              name="country"
              className="form-control"
              id="board-name"
              onChange={this.handleChange}
              placeholder="Birth Country Here"
            />
          </div>



          <button type="submit" className="btn btn-primary m-4">
            Submit
          </button>
        </form>
        </div>)
    }
}
 
export default Update;