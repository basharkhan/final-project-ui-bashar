import React, { Component } from "react";

import * as myApi from "./api.jsx";

import { Link } from "react-router-dom";


import Director from "./Director";

import "./Director.css"

class Directors extends React.Component {
  constructor(props) {
    super(props);
    console.log({props})
    this.state = {
      directors: [],
      name: "",
      oscar: null,
      total_movies: 0,
      country: ""
    };
  }

  // Section for creating director

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = async (event) => {
    event.preventDefault();

    const resp = await myApi.createDirector(this.state);
    this.setState({
      directors: [...this.state.directors, resp],  name: "",
      oscar: "",
      total_movies: 0,
      country: ""
    });
  };

  // section for fetching directors

  async fetchDirector() {
    const directors = await myApi.getDirectors();

    this.setState({
      directors,
    });
  }

  componentDidMount() {
    this.fetchDirector();
  }


  // section for deleting director

   // section for deleting director

   handleDeleteDirector = async (directorId) => {
     console.log(directorId);
    const deleted = await myApi.deleteDirector(directorId);
    
    this.setState({
      directors: this.state.directors.filter((p) => p.id !== directorId),
    });
  };


  // Section for updating director

  handleSubmitUpdate = async (directorId) => {
    console.log("director id", directorId);
   const updated = await myApi.updateDirector(directorId);
   console.log("update", updated);
   
  //  this.setState({
  //   directors: [...this.state.directors, updated],
  // });
   
 };


 handleChangeUpdate = (e) => {
  this.setState({ [e.target.name]: e.target.value });
};


  
  

  render() {
    const { name,  oscar, total_movies, country, directors } = this.state;
    return (
      <div className="containerdirector">

      <div className="d-flex flex-row flex-wrap justify-content-around ">
        {directors.map(({ name, id, oscar, total_movies, country }) => (
          <div>
          <Director
          
            name={name}
            id={id}
            key={id}
            oscar={oscar}
            total_movies={total_movies}
            country={country}
            onDeleteDirector={this.handleDeleteDirector}
            onHandleSubmitUpdate={this.handleSubmitUpdate}
            onHandleChangeUpdate={this.handleChangeUpdate}
          />

          
          </div>
          
        ))}

        <form onSubmit={this.handleSubmit}>
          <div className="mb-3" >
            <label style={{color:"white" }} htmlFor="Director-name">Add Director attributes</label>
            <input
              type="text"
              value={name}
              name="name"
              className="form-control"
              id="board-name"
              onChange={this.handleChange}
              placeholder="Director Name here"
            />
          </div>
          <div className="mb-3">
            <input
              type="text"
              value={oscar}
              name="oscar"
              className="form-control"
              id="board-name"
              onChange={this.handleChange}
              placeholder="Oscar wins here"
            />
          </div>

          <div className="mb-3">
            <input
              type="text"
              value={total_movies}
              name="total_movies"
              className="form-control"
              id="board-name"
              onChange={this.handleChange}
              placeholder="Total Movies here"
            />
          </div>
          
          <div className="mb-3">
            <input
              type="text"
              value={country}
              name="country"
              className="form-control"
              id="board-name"
              onChange={this.handleChange}
              placeholder="Birth Country Here"
            />
          </div>



          <button type="submit" className="btn btn-primary m-4">
            Submit
          </button>
        </form>
      </div>
      </div>
    );
  }
}

export default Directors;



/* <div className="mb-3 m-4">
<input
  type="text"
  value={producer}
  name="producer"
  className="form-control"
  id="board-name"
  onChange={this.handleChange}
  placeholder="Is Producer or not"
/>
</div> */