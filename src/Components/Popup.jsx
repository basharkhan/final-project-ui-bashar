import React, { Component } from 'react';

import * as myApi from "./api";

class Popup extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      directors: [],
      name: "",
      oscar: null,
      total_movies: 0,
      country: ""
    };
  }


  async fetchDirector() {
    const directors = await myApi.getOneDirector(this.props.id);
    console.log(directors);

    // this.setState({
    //   directors,
    // });
  }

  componentDidMount() {
    this.fetchDirector();
  }



  
    render() { 
      console.log("asdasdas",this.props.id);
        return (
<div>

<button  type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
  Edit
</button>

<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Update</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">

      <form onSubmit={ () =>  this.props.onHandleSubmitUpdate(this.props.id)}>
          <div className="mb-3" >
            <label style={{color:"white" }} htmlFor="Director-name">Add Director attributes</label>
            <input
              type="text"
              value={this.props.name}
              name="name"
              className="form-control"
              id="board-name"
              onChange={this.props.onHandleChangeUpdate}
              placeholder="Director Name here"
            />
          </div>
          <div className="mb-3">
            <input
              type="text"
              value={this.props.oscar}
              name="oscar"
              className="form-control"
              id="board-name"
              onChange={this.props.onHandleChangeUpdate}
              placeholder="Oscar wins here"
            />
          </div>

          <div className="mb-3">
            <input
              type="text"
              value={this.props.total_movies}
              name="total_movies"
              className="form-control"
              id="board-name"
              onChange={this.props.onHandleChangeUpdate}
              placeholder="Total Movies here"
            />
          </div>
          
          <div className="mb-3">
            <input
              type="text"
              value={this.props.country}
              name="country"
              className="form-control"
              id="board-name"
              onChange={this.props.onHandleChangeUpdate}
              placeholder="Birth Country Here"
            />
          </div>



          <button type="submit" className="btn btn-primary m-4">
            Submit
          </button>
        </form>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

</div>
            
        )
    }
}
 
export default Popup;