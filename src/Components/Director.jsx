import React, { Component } from "react";

import { Card, ListGroup, Button } from "react-bootstrap";

import { Link } from "react-router-dom";

import './Director.css'

import Popup from "./Popup";
// import steven from "../steven-spielberg.jpg"

const Director = ({ name, id, oscar, total_movies, country, onHandleList, onDeleteDirector, onHandleSubmitUpdate, onHandleChangeUpdate }) => {
  return (
    

/* <Card border="primary" className="m-4" style={{ width: '18rem' }}>
<Card.Header>{name}</Card.Header>
<Card.Body>
  <Card.Title>{oscar}</Card.Title>
  <Card.Text>
    Producer
  </Card.Text>
</Card.Body>
</Card> */

<div className="">

<div className="card border-success mb-3" >
<div className="d-flex justify-content-between">

        <div className="card-header bg-transparent border-success">Movies: {total_movies}</div>
 <Button onClick={() => onDeleteDirector(id)} className="crossbtn" variant="danger">x</Button>
</div>
        <div className="card-body text-success">
          <h5 className="card-title">{name}</h5>
          <p style={{color:"black"}} className="card-text">Birth: {country}
          </p>
        </div>
        <div className="card-footer bg-transparent border-success">Oscar wins: {oscar}</div>
         <div className="">


 <Link key={id} directorId={id} to={`/${id}`}>
 <Button  className="m-4" variant="primary">See Movies</Button>
 </Link>
 </div>

      </div>
</div>

// Earlier Real one start here
//   <div className="d-flex flex-column">

//  <div className="card"  style={{width: "18rem"}}>
//   <div className="card-header">
//   {name}
//   </div>
//   <ul className="list-group list-group-flush">
//     <li className="list-group-item">Oscar Wins: {oscar}</li>
//   </ul>
  
// </div>
// <div className="d-flex">

// <Button onClick={() => onUpdateDirector(id)} className="m-4" variant="info">Edit</Button>
// <Button onClick={() => onDeleteDirector(id)} className="m-4" variant="danger">Delete</Button>
// <Link key={id} directorId={id} to={`/${id}`}>
// <Button  className="m-4" variant="primary">Go in</Button>
// </Link>
// </div>
//   </div>  

// Earlier real one end here


/* <div className="card" style={{width: "18rem"}}>
<img className="card-img-top" src={steven} alt="Card image cap" />
<div className="card-body">
<h5 className="card-title">Card title</h5>
    <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    <a href="#" className="btn btn-primary">Go somewhere</a>
  </div>
  </div> */
  
  );
};

export default Director;

/* <Button className="m-4" variant="info">Edit</Button> */
/* <Popup  id={id} name={name} oscar={oscar} total_movies={total_movies} country={country}  onHandleChangeUpdate={onHandleChangeUpdate} onHandleSubmitUpdate={onHandleSubmitUpdate} /> */